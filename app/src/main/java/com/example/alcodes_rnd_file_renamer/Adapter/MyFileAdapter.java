package com.example.alcodes_rnd_file_renamer.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.Database.MyFile;
import com.example.alcodes_rnd_file_renamer.R;

import java.util.ArrayList;


public class MyFileAdapter extends RecyclerView.Adapter<MyFileAdapter.ViewHolder> {

    //All methods in this adapter are required for a bare minimum recyclerview adapter
    private Context mContext;
    private Cursor mCursor;
    public  ArrayList<MyFile> myFileList;
    String name, size, uri;


    public MyFileAdapter(Context context, ArrayList<MyFile> fileList) {
        mContext = context;
        myFileList = fileList;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nameText;
        public TextView sizeText;
        public TextView sizerange;
        public ImageView checkicon;

        public ViewHolder(View itemView) {
            super(itemView);

            nameText = itemView.findViewById(R.id.Tv_FileName);
            sizeText = itemView.findViewById(R.id.Tv_FileSize);
            checkicon = itemView.findViewById(R.id.checkicon);
            sizerange=itemView.findViewById(R.id.sizerange);

        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_file_list, parent, false);

        return new ViewHolder(view);
    }





    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
       holder.nameText.setText(myFileList.get(position).getFileName());
       int newsize=Integer.parseInt(myFileList.get(position).getFileSize());
       if(newsize>1024){
          newsize=newsize/1024;
          holder.sizerange.setText("MB");
       }
        holder.sizeText.setText(String.valueOf(newsize));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {



                if (holder.checkicon.getVisibility() == View.VISIBLE) {
                    holder.checkicon.setVisibility(View.GONE);
                 myFileList.get(position).setIsSelected(false);

                } else {
                    // Either gone or invisible
                    holder.checkicon.setVisibility(View.VISIBLE);
                    myFileList.get(position).setIsSelected(true);

                }

                notifyDataSetChanged();


            }
        });
    }

    @Override
    public int getItemCount() {
        return myFileList.size();
    }





}





