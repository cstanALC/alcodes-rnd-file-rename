package com.example.alcodes_rnd_file_renamer.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.Database.MyFile;
import com.example.alcodes_rnd_file_renamer.R;

import java.util.ArrayList;

public class previewAdapter extends RecyclerView.Adapter<previewAdapter.ViewHolder> {
    public ArrayList<MyFile> myFileList;
    private Context mContext;
    public ArrayList newnamelist;

    public previewAdapter(Context context, ArrayList<MyFile> FileList, ArrayList strlist) {
        this.mContext = context;
        this.myFileList = FileList;
        this.newnamelist = strlist;
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_preview_list, parent, false);

        return new ViewHolder(view);

    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.oldnameText.setText("Old Name: " + myFileList.get(position).getFileName());
  /*      if (myFileList.get(position).getNewname()==null) {
            holder.newnameText.setText("(Rename first)");

        } else {
            holder.newnameText.setText("New Name: " + myFileList.get(position).getNewname());
        }

    */

       // if (!newnamelist.isEmpty() && newnamelist.get(position) != "") {
        if (!newnamelist.isEmpty() && newnamelist.size()>position) {

            holder.newnameText.setText("New Name: " + newnamelist.get(position));


        } else {
            holder.newnameText.setText("(Rename first)");

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkicon.getVisibility() == View.VISIBLE) {
                    holder.checkicon.setVisibility(View.GONE);
                    myFileList.get(position).setIsSelected(false);

                } else {
                    // Either gone or invisible
                    holder.checkicon.setVisibility(View.VISIBLE);
                    myFileList.get(position).setIsSelected(true);


                }


                notifyDataSetChanged();
            }
        });


    }


    @Override
    public int getItemCount() {
        if (myFileList != null)
            return myFileList.size();
        else return 0;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }




    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView oldnameText;
        public TextView newnameText;
        public ImageView checkicon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            oldnameText = itemView.findViewById(R.id.oldfilename);
            newnameText = itemView.findViewById(R.id.newfilename);
            checkicon = itemView.findViewById(R.id.checkpreview);
        }
    }
}
