package com.example.alcodes_rnd_file_renamer.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.Adapter.previewAdapter;
import com.example.alcodes_rnd_file_renamer.Database.MyFile;
import com.example.alcodes_rnd_file_renamer.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class TaskList extends AppCompatActivity {
    private previewAdapter mAdapter;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_TaskList:
                    setContentView(R.layout.activity_task_list2);
                    Intent intent2 = new Intent(TaskList.this, TaskList.class);
                    intent2.putExtra("filelist", myFileList);
                    intent2.putExtra("newnamelist", newnamelist);
                    finish();
                    overridePendingTransition(0, 0);
                    overridePendingTransition(0, 0);
                    startActivity(intent2);
                    return true;
                case R.id.menu_TaskPreview:

                    setContentView(R.layout.activity_preview_task);
                    changelayout();
                    showitem();
                    ispreview = true;
                    return true;


            }
            return false;
        }
    };
    ArrayList<MyFile> myFileList;
    String newname;
    EditText ETnew, ETreplacechar, ETinsertchar, ETinsertnumber, ETinsertSize, ETreplacechar2;


    Button renamebtn, finishbtn, finishrenamebtn;
    ImageView closebtn, closebtnP;
    ArrayList newnamelist = new ArrayList();
    RelativeLayout RtaskLayout;
    private Menu previewmenu;
    Uri dirpath;
    Button changedir;
    Boolean ispreview = false;
    Boolean isNewname = false;
    Boolean isedit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list2);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        finishrenamebtn = findViewById(R.id.finishrenamebtn);
        ETnew = findViewById(R.id.NewnameET);
        ETreplacechar = findViewById(R.id.RcharET);
        ETinsertchar = findViewById(R.id.insertchar);
        ETinsertnumber = findViewById(R.id.insertnumb);
        ETreplacechar2 = findViewById(R.id.ETReplaceC);

        myFileList = (ArrayList<MyFile>) getIntent().getSerializableExtra("filelist");

        if ((ArrayList) getIntent().getSerializableExtra("newnamelist") != null)
            newnamelist = (ArrayList) getIntent().getSerializableExtra("newnamelist");

        isedit = getIntent().getExtras().getBoolean("edittask");

        closebtn = findViewById(R.id.closebtn);
        closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(TaskList.this, TaskList.class);
                intent2.putExtra("filelist", myFileList);
                intent2.putExtra("newnamelist", newnamelist);

                startActivity(intent2);
            }
        });


        RtaskLayout = findViewById(R.id.RenameTask);
        renamebtn = findViewById(R.id.renamerbtn);
        renamebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
                builder.setTitle(R.string.selectoption);
                builder.setMessage(R.string.selectoption1);


                builder.setPositiveButton(R.string.Oldname, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isNewname = false;
                        renamebtn.setVisibility(View.GONE);
                        RtaskLayout.setVisibility(View.VISIBLE);
                        setoldnamelayout();


                    }
                });
                builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });
                builder.setNegativeButton(R.string.newname, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        renamebtn.setVisibility(View.GONE);

                        RtaskLayout.setVisibility(View.VISIBLE);
                        setnewnamelayout();
                        isNewname = true;

                    }
                });


                builder.show();


            }
        });


        finishrenamebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNewname) {
                    ToRenaming();
                    isedit = false;
                } else {
                    Torenameold();
                    isedit = false;
                }
            }
        });

        if (isedit == true) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
            builder.setTitle(R.string.selectoption);
            builder.setMessage(R.string.selectoption1);


            builder.setPositiveButton(R.string.Oldname, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    isNewname = false;
                    renamebtn.setVisibility(View.GONE);
                    RtaskLayout.setVisibility(View.VISIBLE);
                    setoldnamelayout();


                }
            });
            builder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();
                }
            });
            builder.setNegativeButton(R.string.newname, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    renamebtn.setVisibility(View.GONE);

                    RtaskLayout.setVisibility(View.VISIBLE);
                    setnewnamelayout();
                    isNewname = true;

                }
            });


            builder.show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case 42:
                dirpath = data.getData();

                break;

        }

    }


    public void setoldnamelayout() {

        ETnew.setVisibility(View.GONE);
        TextView Tvnew;
        Tvnew = findViewById(R.id.Tvnew);
        Tvnew.setVisibility(View.GONE);
    }


    public void setnewnamelayout() {

        ETreplacechar.setVisibility(View.GONE);
        ETinsertchar.setVisibility(View.GONE);
        TextView TV1, TV2, TV3, TV4;
        TV1 = findViewById(R.id.Tv_RC);
        TV2 = findViewById(R.id.Tv_RC2);
        ETreplacechar2.setVisibility(View.GONE);
        TV3 = findViewById(R.id.Tv_IC);
        TV4 = findViewById(R.id.Tv_IC2);
        TV1.setVisibility(View.GONE);
        TV2.setVisibility(View.GONE);
        TV3.setVisibility(View.GONE);
        TV4.setVisibility(View.GONE);


    }

    public String getExt(String filePath) {
        int strLength = filePath.lastIndexOf(".");
        if (strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }

    public String getSizeFromname(String file) {
        int strLength = file.lastIndexOf("(");
        if (strLength > 0)
            return file.substring(strLength + 1);
        return null;


    }

    MenuItem clearall;
    MenuItem removeone;
    MenuItem addtask;
    MenuItem Edittask;


    @Override
    public void onBackPressed() {
        if (ispreview == true) {
            Intent intent2 = new Intent(TaskList.this, TaskList.class);
            intent2.putExtra("filelist", myFileList);
            intent2.putExtra("newnamelist", newnamelist);

            startActivity(intent2);

        } else {
            Intent intent2 = new Intent(TaskList.this, FileList.class);
            intent2.putExtra("filelist", myFileList);
            intent2.putExtra("newnamelist", newnamelist);

            startActivity(intent2);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_previewlist, menu);
        clearall = menu.findItem(R.id.clearall);
        removeone = menu.findItem(R.id.removeone);
        addtask = menu.findItem(R.id.addtask);
        Edittask = menu.findItem(R.id.Edittask);
        hideItem();
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clearall:
                toClearall();
                return true;
            case R.id.removeone:
                toremovetask();
                return true;
            case R.id.addtask:
                Intent intent = new Intent(TaskList.this, FileList.class);
                intent.putExtra("filelist", myFileList);
                intent.putExtra("newnamelist", newnamelist);
                startActivity(intent);
                return true;
            case R.id.Edittask:
                Intent intent2 = new Intent(TaskList.this, TaskList.class);
                intent2.putExtra("filelist", myFileList);
                intent2.putExtra("newnamelist", newnamelist);
                intent2.putExtra("edittask", true);
                startActivity(intent2);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void toClearall() {

        AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
        builder.setTitle(R.string.confirm);
        builder.setMessage(R.string.clearmsg);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                for (int i = 0; i < myFileList.size(); i++) {
                    myFileList.remove(i);

                }
                for (int i = 0; i < newnamelist.size(); i++) {
                    newnamelist.remove(i);
                }
                Intent back = new Intent(getApplicationContext(), FileList.class);
                finish();
                overridePendingTransition(0, 0);
                startActivity(back);
                overridePendingTransition(0, 0);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        builder.show();


    }

    public void toremovetask() {
        AlertDialog.Builder builder2 = new AlertDialog.Builder(TaskList.this);
        builder2.setTitle(R.string.confirm);
        builder2.setMessage(R.string.removemsg);


        builder2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < myFileList.size(); i++) {
                    if (myFileList.get(i).getIsSelected() == true) {
                        myFileList.remove(i);
                        if (newnamelist != null && newnamelist.size() > 1)
                            newnamelist.remove(i);
                    }
                }

                mAdapter.notifyDataSetChanged();

            }
        });
        builder2.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        builder2.show();


    }


    public void Torenameold() {
        int insrtnumber;
        String replacechar;
        String replacechar2;
        String insertchar;
        String namewithsize = "";
        int s = 0;
        String size = "";
        String separator = ".";
        Boolean Checkinsert = true;
        if (ETreplacechar.getText().toString().isEmpty() && ETreplacechar2.getText().toString().isEmpty()) {
            replacechar = "";
            replacechar2 = "";
        } else {
            replacechar = ETreplacechar.getText().toString();
            replacechar2 = ETreplacechar2.getText().toString();
        }


        if (ETinsertchar.getText().toString().isEmpty()) {
            insertchar = "";
        } else {
            insertchar = ETinsertchar.getText().toString();
            Checkinsert = isFilenameValid(insertchar);
        }

        if (ETinsertnumber.getText().toString().isEmpty()) {
            insrtnumber = 1;
        } else {
            insrtnumber = Integer.valueOf(ETinsertnumber.getText().toString());
        }

        if (Checkinsert == false) {
            showerror("invalidname");
        }


        if (Checkinsert == true) {


            if (isedit != true) {


                //Replace old name
                for (int i = 0; i < myFileList.size(); i++) {
                    if (ischecked) {
                        s = Integer.valueOf(myFileList.get(i).getFileSize());
                        if (s > 1024) {
                            s = s / 1024;
                            size = "(" + s + "MB" + ")";
                        } else {
                            size = "(" + s + "KB" + ")";

                        }
                        ischecked = false;
                    }


                    if (i == 0) {


                        String name = myFileList.get(i).getFileName();
                        //take ext
                        int sepPos = name.lastIndexOf(separator);
                        if (sepPos != -1) {
                            name = name.substring(0, sepPos);
                            namewithsize = name;
                        }

                        //take size str
                        if (name.contains("(")) {
                            int sep = name.lastIndexOf("(");
                            if (sep != -1) {
                                name = name.substring(0, sep);
                            }
                        }


                        if (replacechar != null && replacechar2 != null) {
                            name = name.replace(replacechar, replacechar2);
                        }
                        String ex = "." + getExt(myFileList.get(i).getFileName());
                    /*if(name.contains("KB")||name.contains("MB"))
                    {
                        size="";
                    }*/
                        if (namewithsize.contains("(")) {
                            size = "(" + getSizeFromname(namewithsize);
                        }


                        if (!newnamelist.isEmpty() && newnamelist.size() > i)

                            newnamelist.set(i, name + insertchar + size + ex);

                        else
                            newnamelist.add(name + insertchar + size + ex);


                    } else {
                        String name = myFileList.get(i).getFileName();
                        int sepPos = name.lastIndexOf(separator);
                        if (sepPos != -1) {
                            name = name.substring(0, sepPos);
                            namewithsize = name;

                        }
                        //take size str
                        if (name.contains("(")) {
                            int sep = name.lastIndexOf("(");
                            if (sep != -1) {
                                name = name.substring(0, sep);

                            }
                        }

                        name += insertchar;
                        if (replacechar != null && replacechar2 != null) {
                            name = name.replace(replacechar, replacechar2);
                        }

                        int cal = insrtnumber * i;
                        String totalindex = " " + String.valueOf(cal);
                        String ex = "." + getExt(myFileList.get(i).getFileName());
               /*     if(name.contains("KB")||name.contains("MB"))
                    {
                        size="";
                    }*/
                        if (namewithsize.contains("(")) {
                            size = "(" + getSizeFromname(namewithsize);
                        }


                        if (!newnamelist.isEmpty() && newnamelist.size() > i)


                            newnamelist.set(i, name + totalindex + size + ex);

                        else
                            newnamelist.add(name + totalindex + size + ex);

                    }


                }
            }
            //Edit
            else {
                int countedit = 0;
                for (int i = 0; i < myFileList.size(); i++) {

                    if (myFileList.get(i).getIsSelected()) {
                        if (ischecked) {
                            s = Integer.valueOf(myFileList.get(i).getFileSize());
                            if (s > 1024) {
                                s = s / 1024;
                                size = "(" + s + "MB" + ")";
                            } else {
                                size = "(" + s + "KB" + ")";

                            }
                            ischecked = false;
                        }


                        if (countedit == 0) {


                            String name = myFileList.get(i).getFileName();
                            //take ext
                            int sepPos = name.lastIndexOf(separator);
                            if (sepPos != -1) {
                                name = name.substring(0, sepPos);
                                namewithsize = name;
                            }

                            //take size str
                            if (name.contains("(")) {
                                int sep = name.lastIndexOf("(");
                                if (sep != -1) {
                                    name = name.substring(0, sep);
                                }
                            }


                            if (replacechar != null && replacechar2 != null) {
                                name = name.replace(replacechar, replacechar2);
                            }
                            String ex = "." + getExt(myFileList.get(i).getFileName());
                    /*if(name.contains("KB")||name.contains("MB"))
                    {
                        size="";
                    }*/
                            if (namewithsize.contains("(")) {
                                size = "(" + getSizeFromname(namewithsize);
                            }


                            if (!newnamelist.isEmpty() && newnamelist.size() > i)

                                newnamelist.set(i, name + insertchar + size + ex);

                            else
                                newnamelist.add(name + insertchar + size + ex);
                            countedit++;

                        } else {
                            String name = myFileList.get(i).getFileName();
                            int sepPos = name.lastIndexOf(separator);
                            if (sepPos != -1) {
                                name = name.substring(0, sepPos);
                                namewithsize = name;

                            }
                            //take size str
                            if (name.contains("(")) {
                                int sep = name.lastIndexOf("(");
                                if (sep != -1) {
                                    name = name.substring(0, sep);

                                }
                            }

                            name += insertchar;
                            if (replacechar != null && replacechar2 != null) {
                                name = name.replace(replacechar, replacechar2);
                            }

                            int cal = insrtnumber * countedit;
                            String totalindex = " " + String.valueOf(cal);
                            String ex = "." + getExt(myFileList.get(i).getFileName());
               /*     if(name.contains("KB")||name.contains("MB"))
                    {
                        size="";
                    }*/
                            if (namewithsize.contains("(")) {
                                size = "(" + getSizeFromname(namewithsize);
                            }


                            if (!newnamelist.isEmpty() && newnamelist.size() > i) {
                                newnamelist.set(i, name + totalindex + size + ex);
                            } else {

                                newnamelist.add(name + totalindex + size + ex);
                            }
                            countedit++;
                        }
                    }


                }


            }


            AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
            builder.setTitle(R.string.done);
            builder.setMessage(R.string.msgpreview);


            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    setContentView(R.layout.activity_preview_task);
                    changelayout();
                    ispreview = true;

                }
            });
            builder.show();
        }
    }

    public static boolean isFilenameValid(String file) {
        String namepattern = "^[\\p{L} .'-]+$";
        if (file.matches(namepattern)) {
            return true;
        }
        return false;
    }


    public void ToRenaming() {
        String rename = ETnew.getText().toString();
        int insrtnumber;
        int finalsize;
        Boolean NameRepeated = false;
        Boolean StrValidate = true;
        int s = 0;
        String size = "";
        //checking duplicate name


        if (ETinsertnumber.getText().toString().isEmpty()) {
            insrtnumber = 1;
        } else {
            insrtnumber = Integer.valueOf(ETinsertnumber.getText().toString());
        }
        //Replace new name
        if (isedit != true) {
            if (isNewname = true) {
                StrValidate = isFilenameValid(rename);
                if (StrValidate == false) {
                    showerror("invalidname");
                } else {
                    for (int i = 0; i < myFileList.size(); i++) {
                        String fullname = rename;
                        String ex = "." + getExt(myFileList.get(i).getFileName());

                        if (ischecked) {

                            s = Integer.valueOf(myFileList.get(i).getFileSize());
                            if (s > 1024) {
                                s = s / 1024;
                                size = "(" + s + "MB" + ")";
                            } else {
                                size = "(" + s + "KB" + ")";

                            }
                            ischecked = false;

                        }

                        //check repeat name from text field


                        if (i == 0) {
                            String full = fullname + size + ex;
                            if (full.equals(myFileList.get(i).getFileName())) {
                                NameRepeated = true;
                            }

                            if (!newnamelist.isEmpty() && newnamelist.size() > i)

                                newnamelist.set(i, full);

                            else
                                newnamelist.add(full);

                        } else {

                            int cal = insrtnumber * i;
                            String totalindex = " " + String.valueOf(cal);
                            String full = fullname + totalindex + size + ex;
                            if (full.equals(myFileList.get(i).getFileName())) {
                                NameRepeated = true;
                            }


                            if (!newnamelist.isEmpty() && newnamelist.size() > i)

                                newnamelist.set(i, full);

                            else
                                newnamelist.add(full);

                        }


                    }
                }

            }
        }
        //Edit
        if (isedit) {
            if (isNewname = true) {
                StrValidate = isFilenameValid(rename);
                if (StrValidate == false) {
                    showerror("invalidname");
                } else {
                    int count = 0;

                    for (int i = 0; i < myFileList.size(); i++) {
                        String fullname = rename;
                        String ex = "." + getExt(myFileList.get(i).getFileName());

                        if (myFileList.get(i).getIsSelected()) {
                            if (ischecked) {

                                s = Integer.valueOf(myFileList.get(i).getFileSize());
                                if (s > 1024) {
                                    s = s / 1024;
                                    size = "(" + s + "MB" + ")";
                                } else {
                                    size = "(" + s + "KB" + ")";

                                }
                                ischecked = false;

                            }

                            if (count == 0) {

                                //  myFileList.get(i).setNewname(fullname + ex);
                                String full = fullname + size + ex;
                                //   newnamelist.set(i, full);
                                if (full.equals(myFileList.get(i).getFileName()))
                                    NameRepeated = false;
                                if (!newnamelist.isEmpty() && newnamelist.size() > i)

                                    newnamelist.set(i, full);

                                else
                                    newnamelist.add(full);


                                myFileList.get(i).setIsSelected(false);
                                count++;
                            } else {

                                int cal = insrtnumber * count;
                                String totalindex = " " + String.valueOf(cal);
                                String full = fullname + totalindex + size + ex;
                                if (full.equals(myFileList.get(i).getFileName()))
                                    NameRepeated = false;
                                if (!newnamelist.isEmpty() && newnamelist.size() > i)
                                    newnamelist.set(i, full);
                                else
                                    newnamelist.add(full);
                                myFileList.get(i).setIsSelected(false);

                                count++;
                            }
                        }


                    }
                }

            }

        }
        if (NameRepeated == true) {

            AlertDialog.Builder builder2 = new AlertDialog.Builder(TaskList.this);
            builder2.setTitle(R.string.repeatname);

            builder2.setMessage(R.string.repeatname);


            builder2.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Intent intent = new Intent(TaskList.this, TaskList.class);
                    intent.putExtra("filelist", myFileList);
                    finish();
                    overridePendingTransition(0, 0);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });
            builder2.show();
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
        builder.setTitle(R.string.done);

        builder.setMessage(R.string.msgpreview);


        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                setContentView(R.layout.activity_preview_task);
                changelayout();
                ispreview = true;

            }
        });
        if (StrValidate == true && NameRepeated == false) {
            builder.show();
        }

    }

    private void hideItem() {
        clearall.setVisible(false);
        removeone.setVisible(false);
        addtask.setVisible(false);
        Edittask.setVisible(false);

    }

    public void showitem() {
        clearall.setVisible(true);
        removeone.setVisible(true);
        addtask.setVisible(true);
        Edittask.setVisible(true);

    }


    public void pickdir() {

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);


        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.putExtra("android.content.extra.FANCY", true);
        intent.putExtra("android.content.extra.SHOW_FILESIZE", true);

        startActivityForResult(intent, 42);

    }

    public void tomovefile(Uri src) throws FileNotFoundException {
        String docId = DocumentsContract.getTreeDocumentId(dirpath);
        Uri dirUri = DocumentsContract.buildDocumentUriUsingTree(dirpath, docId);


      DocumentsContract.moveDocument(getContentResolver(),src,src,dirUri);
    }





    public void finishtask() throws IOException {


        //rename
        Date lastModified = null;
        Uri moveornot = null;
        Uri complteuri = null, copy = null;
        Long id = null;
        Boolean complte = false, modifed = false;

        for (int i = 0; i < newnamelist.size(); i++) {
            String newname = newnamelist.get(i).toString();
            Uri uri = Uri.parse(myFileList.get(i).getFileUri());

            if (dirpath != null) {


                File ori = new File(uri.getPath());
                long oril = ori.lastModified();


                complteuri = renameTo(getApplicationContext(), uri, newname);
                tomovefile(complteuri);


            } else {
                complteuri = renameTo(getApplicationContext(), uri, newname);

            }

            if (complteuri == null)
                complte = false;
            else

                complte = true;
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(TaskList.this);
        builder.setTitle(R.string.done);
        builder.setMessage(R.string.renamefinish);


        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent2 = new Intent(TaskList.this, FileList.class);

                startActivity(intent2);
            }
        });
       /* if (dirpath != null) {
            if (copy != null) {
                builder.show();
            }
            dirpath = null;
        } else if (complte == true) {
            builder.show();

        }
        */
        builder.show();
    }


    public void showerror(String errortype) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.fail);
        if (errortype == "invalidname") {
            builder.setMessage(R.string.invalidname);
            builder.setPositiveButton(R.string.ok, null);

        }
        if (errortype == "new dir") {
            builder.setMessage(R.string.invaliddir);
            builder.setPositiveButton(R.string.ok, null);

        } else {
            builder.setMessage(R.string.repeatname);
            builder.setPositiveButton(R.string.ok, null);

        }

        builder.show();
    }

    public Uri renameTo(final Context context, Uri self, String displayName) {
        try {
            return DocumentsContract.renameDocument(context.getContentResolver(), self, displayName);

        } catch (FileNotFoundException e) {
            // Maybe user ejects tf card
            e.printStackTrace();

            showerror("");

            return null;
        }

    }

    public void changelayout() {
        showitem();

        finishbtn = findViewById(R.id.finishbtn);
        finishbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    finishtask();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        changedir = findViewById(R.id.ChangeDir);
        changedir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickdir();
            }
        });


        assginadapater();

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        if (newnamelist.isEmpty()) {
            finishbtn.setVisibility(View.INVISIBLE);
        } else if (newnamelist.get(0).equals(""))
            finishbtn.setVisibility(View.INVISIBLE);


    }

    public void assginadapater() {


        RecyclerView recyclerView = findViewById(R.id.PreviewRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new previewAdapter(this, myFileList, newnamelist);
        recyclerView.setAdapter(mAdapter);


    }

    Boolean ischecked = false;

    public void checkboxclicked(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (checkBox.isChecked()) {
            ischecked = true;
            checkBox.setSelected(false);
        }
    }
}















