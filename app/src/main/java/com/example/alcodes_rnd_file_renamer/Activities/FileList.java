package com.example.alcodes_rnd_file_renamer.Activities;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.Adapter.MyFileAdapter;
import com.example.alcodes_rnd_file_renamer.Database.MyFile;
import com.example.alcodes_rnd_file_renamer.R;

import java.io.Closeable;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FileList extends AppCompatActivity {
    private MyFileAdapter mAdapter;
    int fileposition = 0;
    TextView tv_nodata;
    String newname;
    private static final int OPEN_REQUEST_CODE = 41;

    Button ChooseBtn;
    Button switchlist;
    Button ChooeseDir;
    ArrayList newnamelist = new ArrayList();
    ArrayList<String> urilist = new ArrayList<>();
    ArrayList<MyFile> myFileList = new ArrayList<MyFile>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filelist);
        ChooseBtn = findViewById(R.id.choosefile);
        ChooeseDir = findViewById(R.id.chooseDirectory);
        tv_nodata = findViewById(R.id.msgFile);
        switchlist = findViewById(R.id.buttonswitch);


        ChooseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenFileManager();


            }
        });
        ChooeseDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDirectory();
            }
        });

        switchlist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FileList.this, TaskList.class);
                intent.putExtra("filelist", myFileList);
                intent.putExtra("newnamelist", newnamelist);
                startActivity(intent);
            }
        });
        //from tasklist back to file list
        if ((ArrayList) getIntent().getSerializableExtra("filelist") != null) {
            myFileList = (ArrayList<MyFile>) getIntent().getSerializableExtra("filelist");
            assginadapater();
            mAdapter.notifyDataSetChanged();

        }
        if ((ArrayList) getIntent().getSerializableExtra("newnamelist") != null) {
            newnamelist = (ArrayList) getIntent().getSerializableExtra("newnamelist");
        }

    }

    public void assginadapater() {
        if (myFileList.isEmpty()) {
            tv_nodata.setVisibility(View.VISIBLE);
            switchlist.setVisibility(View.INVISIBLE);

        }
        else {
            tv_nodata.setVisibility(View.GONE);
            switchlist.setVisibility(View.VISIBLE);

        }

        RecyclerView recyclerView = findViewById(R.id.FileRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MyFileAdapter(this, myFileList);
        recyclerView.setAdapter(mAdapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_fielist, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.removelist:
                deleteitem();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    Boolean doubleclick = false;

    @Override
    public void onBackPressed() {
        if (doubleclick) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
        this.doubleclick = true;
        Toast.makeText(this, R.string.click_back, Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleclick = false;
            }
        }, 2000);


    }

    public void OpenFileManager() {


        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        //testing

        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
     intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.putExtra("android.content.extra.FANCY", true);
       intent.putExtra("android.content.extra.SHOW_FILESIZE", true);
        startActivityForResult(intent, OPEN_REQUEST_CODE);
    }

    public void openDirectory() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);

        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
        intent.putExtra("android.content.extra.FANCY", true);
        intent.putExtra("android.content.extra.SHOW_FILESIZE", true);

        startActivityForResult(intent, 42);
    }


    public void getDirDetails(Uri TreeUri) {


        DocumentFile documentFile = DocumentFile.fromTreeUri(this, TreeUri);
        for (DocumentFile file : documentFile.listFiles()) {
            long sizeIndex = file.length();

            String size = String.valueOf(sizeIndex);
            Double mSize = 1 + Double.valueOf(size) / 1024.0;
            long iSize = Math.round(mSize);

            MyFile newfile = new MyFile(file.getName(), String.valueOf(iSize), String.valueOf(file.getUri()));
            String extension = getExt(file.getName());


            myFileList.add(newfile);
           // newnamelist.add("");

            assginadapater();
            switchlist.setVisibility(View.VISIBLE);


        }
    }


    public String getExt(String filePath) {
        int strLength = filePath.lastIndexOf(".");
        if (strLength > 0)
            return filePath.substring(strLength + 1).toLowerCase();
        return null;
    }

    public void getFileMetaData(Uri uri) {


        AlertDialog.Builder builder = new AlertDialog.Builder(FileList.this);
        builder.setTitle(R.string.selectsame);

        DocumentFile file = DocumentFile.fromSingleUri(getApplicationContext(), uri);

            builder.setMessage(R.string.msgrepeatfile);


        builder.setPositiveButton(R.string.ok, null);
        try {

            if (file != null) {
                String displayName = file.getName();
                long sizeIndex = file.length();

                String size = null;


                if (sizeIndex != 0) {

                    // Technically the column stores an int, but cursor.getString()
                    // will do the conversion automatically.
                    size = String.valueOf(sizeIndex);
                    Double mSize = 1 + Double.valueOf(size) / 1024.0;
                    long iSize = Math.round(mSize);

                    MyFile newfile = new MyFile(displayName, String.valueOf(iSize), String.valueOf(uri));
                    Boolean repeat = false;
                    for (int i = 0; i < myFileList.size(); i++) {
                        String oldname = myFileList.get(i).getFileName();
                        if (oldname.equals(displayName))
                            repeat = true;

                    }
                    if (repeat != true) {
                        myFileList.add(newfile);
                       // newnamelist.add("");
                    } else
                        builder.show();

                } else {
                    size = "Unknown";
                }
            }
        } finally {

        }
        assginadapater();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {


            if (requestCode == OPEN_REQUEST_CODE) {
                if (null != data) {
                    if (null != data.getClipData()) { // checking multiple selection or not

                        for (int i = 0; i < data.getClipData().getItemCount(); i++) {
                            Uri uri = data.getClipData().getItemAt(i).getUri();

                            getFileMetaData(uri);
//change to uri list to store
                            switchlist.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Uri uri = data.getData();
//

                        getFileMetaData(uri);
                        switchlist.setVisibility(View.VISIBLE);
                    }
                }

            }
            if (requestCode == 42) {
                Uri uri = data.getData();

                getDirDetails(uri);

            }
        }
    }
        public Uri traverseDirectoryEntries(Uri uri) {
            ContentResolver contentResolver = this.getContentResolver();
            Uri childrenUri;

            // Keep track of our directory hierarchy
            List<Uri> dirNodes = new LinkedList<>();


            try {

                childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getDocumentId(uri));


            } catch (Exception e) {
                childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));

            }


            return childrenUri;

        }


    public void deleteitem() {
    ArrayList<String> filelist=new ArrayList<String>();
        for (Iterator<MyFile> iter = myFileList.iterator(); iter.hasNext(); ) {
            MyFile file = iter.next();
            if (file.getIsSelected() == true) {
                iter.remove();
            }
        }


        mAdapter.notifyDataSetChanged();

        Intent intent = new Intent(FileList.this, FileList.class);
        intent.putExtra("filelist", myFileList);
        //
        finish();
        overridePendingTransition(0, 0);
        overridePendingTransition(0, 0);
        startActivity(intent);
        //
        if (myFileList.isEmpty()) {
            switchlist.setVisibility(View.GONE);
        }


    }
}
