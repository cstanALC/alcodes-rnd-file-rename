package com.example.alcodes_rnd_file_renamer.Database;

import android.os.Parcel;
import android.os.Parcelable;

public class MyFile implements Parcelable {
private String FileName;
private String FileSize;
private String FileUri;
    private boolean isSelected =false;
    private  boolean toDelete=false;
    private String newname;
    private String FileType;

    public MyFile(String fileName, String fileSize,String fileUri) {
        FileName = fileName;
        FileSize = fileSize;
        FileUri=fileUri;
    }

    public MyFile(Parcel in) {
        FileName = in.readString();
        FileSize = in.readString();
        FileUri = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<MyFile> CREATOR = new Creator<MyFile>() {
        @Override
        public MyFile createFromParcel(Parcel in) {
            return new MyFile(in);
        }

        @Override
        public MyFile[] newArray(int size) {
            return new MyFile[size];
        }
    };

    public String getFileUri() {
        return FileUri;
    }

    public void setFileUri(String fileUri) {
        FileUri = fileUri;
    }
    public boolean getIsSelected(){
        return isSelected;

    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getNewname() {
        return newname;
    }

    public void setNewname(String newname) {
        this.newname = newname;
    }

    public void setIsSelected(boolean selected){
        isSelected=selected;
    }
public boolean getToDelete(){
        return toDelete;
}
public void setToDelete(boolean delete){
        toDelete=delete;
}

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileSize() {
        return FileSize;
    }

    public void setFileSize(String fileSize) {
        FileSize = fileSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(FileName);
        dest.writeString(FileSize);
        dest.writeString(FileUri);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }
}
